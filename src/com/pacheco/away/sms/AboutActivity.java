package com.pacheco.away.sms;

/*
 * Android Project Away SMS
 * Author: Gustavo Pacheco
 * Loyola University Chicago COMP 312 Open Source Computing
 * 
 * This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This application is also under the Creative Commons Attribution-ShareAlike 3.0 
    Unported (CC BY-SA 3.0).
    
    You are free:

	to Share � to copy, distribute and transmit the work
	to Remix � to adapt the work
	to make commercial use of the work
	
	Under the following conditions:

	Attribution � You must attribute the work in the manner specified by the author or licensor 
	(but not in any way that suggests that they endorse you or your use of the work).
	
	Share Alike � If you alter, transform, or build upon this work, you may distribute the 
	resulting work only under the same or similar license to this one.
 */


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;

public class AboutActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_about, menu);
        return true;
    }
    
    public void clickMain(MenuItem item){
    	finish();
    }
}
